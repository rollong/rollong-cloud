package com.rollong.cloud.server.service

import com.rollong.cloud.server.config.TencentCloudConfig
import com.rollong.cloud.server.feign.TencentImFeignClient
import com.rollong.cloud.vendor.tencent.common.im.IMAppConfig
import com.rollong.cloud.vendor.tencent.common.im.IMClient
import com.rollong.cloud.vendor.tencent.common.im.Payload
import com.rollong.cloud.vendor.tencent.common.im.feignclient.TencentCloudImAPI
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/03 12:01
 * @project polar-api
 * @filename SmsService.kt
 * @ide IntelliJ IDEA
 * @package com.polar.user.service.sms
 * @description
 */
@Component
class IMService(
    private val config: TencentCloudConfig,
    private val api: TencentCloudImAPI
) {

    private var logger = LoggerFactory.getLogger(IMService::class.java)

    private val client: IMClient
        get() = IMClient(
            appConfig = IMAppConfig().apply {
                this.administrator = "administrator"
                this.appId = config.im.appId
                this.appKey = config.im.appSecret
                this.expireInSeconds = config.im.expireTime / (60 * 60 * 24)
            },
            api = api
        )

    fun createGroup(
        id: String,
        name: String,
        type: Payload.CreateGroupRequest.GroupType = Payload.CreateGroupRequest.GroupType.ChatRoom
    ) {
        client.createGroup(id, name, type)
    }

    fun destroyGroup(
        id: String
    ) {
        client.destroyGroup(id)
    }
}