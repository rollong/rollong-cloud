package com.rollong.cloud.server

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.openfeign.EnableFeignClients
import org.springframework.scheduling.annotation.EnableAsync
import org.springframework.scheduling.annotation.EnableScheduling

@SpringBootApplication
@EnableAsync
@EnableScheduling
@EnableFeignClients("com.rollong.**.feign")
class ServerApplication

fun main(args: Array<String>) {
    runApplication<ServerApplication>(*args)
}
