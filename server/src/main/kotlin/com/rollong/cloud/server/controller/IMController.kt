package com.rollong.cloud.server.controller

import com.rollong.cloud.server.service.IMService
import com.rollong.cloud.vendor.tencent.common.im.Payload
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*

/**
 * Title：EventController
 * Description：
 * @author Flicker
 * @create 2020/12/1 11:13
 **/
@RestController
@Api(tags = ["腾讯云IM"], description = "腾讯云IM")
@RequestMapping("/tencent_cloud/im")
class IMController(
    private val imService: IMService
) {

    @PostMapping("/create_group")
    @ApiOperation(value = "创建群组")
    fun create(@RequestBody @Validated request: CreateGroupRequest){
        imService.createGroup(
            id = request.id,
            name = request.name,
        )
    }

}

data class CreateGroupRequest(
    var id: String,
    var name: String,
    var type: Payload.CreateGroupRequest.GroupType = Payload.CreateGroupRequest.GroupType.ChatRoom
)