package com.rollong.cloud.server.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component

@Component
@ConfigurationProperties(prefix = "app.tencent-cloud")
class TencentCloudConfig(
    var secretId: String? = null,
    var secretKey: String? = null,
    var trtc: TrtcAppConfig = TrtcAppConfig(),
    var im: IMAppConfig = IMAppConfig(),
    var vod: VodConfig = VodConfig()
) {
    data class TrtcAppConfig(
        var appId: Long = 0,
        var appSecret: String = "",
        var expireTime: Long = 604800
    )

    data class IMAppConfig(
        var appId: Long = 0,
        var appSecret: String = "",
        var expireTime: Long = 604800
    )

    data class VodConfig(
        var appId: Long = 0,
    )
}