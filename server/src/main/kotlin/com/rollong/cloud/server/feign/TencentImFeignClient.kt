package com.rollong.cloud.server.feign

import com.rollong.cloud.vendor.tencent.common.im.feignclient.TencentCloudImAPI
import org.springframework.cloud.openfeign.FeignClient

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/06 01:18
 * @project polar-api
 * @filename SmsFeignclient.kt
 * @ide IntelliJ IDEA
 * @package com.polar.user.feignclient.notify
 * @description
 */
@FeignClient(name = "tencent-cloud-im", url = "https://console.tim.qq.com/")
interface TencentImFeignClient : TencentCloudImAPI