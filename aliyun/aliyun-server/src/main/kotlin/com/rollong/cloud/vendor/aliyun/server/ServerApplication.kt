package com.rollong.cloud.vendor.aliyun.server

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.scheduling.annotation.EnableAsync
import org.springframework.scheduling.annotation.EnableScheduling

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/07 20:23
 * @project rollong-aliyun
 * @filename ServerApplication.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.aliyun.server
 * @description
 */
@SpringBootApplication
@EnableAsync
@EnableScheduling
class ServerApplication

fun main(args: Array<String>) {
    SpringApplication.run(ServerApplication::class.java, *args)
}