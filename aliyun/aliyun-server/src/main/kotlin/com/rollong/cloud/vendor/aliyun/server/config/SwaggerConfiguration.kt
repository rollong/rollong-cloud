package com.rollong.cloud.vendor.aliyun.server.config

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import springfox.documentation.builders.ApiInfoBuilder
import springfox.documentation.builders.PathSelectors
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.service.ApiInfo
import springfox.documentation.service.Contact
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spring.web.plugins.Docket
import springfox.documentation.swagger2.annotations.EnableSwagger2

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/07 20:24
 * @project rollong-aliyun
 * @filename SwaggerConfiguration.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.cloud.vendor.aliyun.server.config
 * @description
 */
@Configuration
@EnableSwagger2
class SwaggerConfiguration {

    @Value("\${swagger.enable}")
    private val swaggerEnabled: Boolean = false

    @Bean(value = ["defaultApi"])
    fun defaultApi(): Docket {
        return Docket(DocumentationType.SWAGGER_2)
            .apiInfo(apiInfo())
            .enable(swaggerEnabled)
            .select()
            .apis(RequestHandlerSelectors.basePackage("com.rollong.aliyun"))
            .paths(PathSelectors.any())
            .build()
    }

    private fun apiInfo(): ApiInfo {
        return ApiInfoBuilder()
            .title("Rollong-Aliyun-Proxy RESTful APIs")
            .contact(Contact("Rollong", "rollong.com", ""))
            .description("Rollong-notify RESTful APIs")
            .termsOfServiceUrl("https://www.rollong.com/")
            .version("1.0")
            .build()
    }
}