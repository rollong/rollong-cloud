package com.rollong.cloud.vendor.aliyun.common.sts

import com.aliyuncs.DefaultAcsClient
import com.aliyuncs.IAcsClient
import com.aliyuncs.http.MethodType
import com.aliyuncs.profile.DefaultProfile
import com.aliyuncs.profile.IClientProfile
import com.aliyuncs.sts.model.v20150401.AssumeRoleRequest
import com.aliyuncs.sts.model.v20150401.AssumeRoleResponse
import com.rollong.cloud.vendor.aliyun.common.response.getResponse
import org.slf4j.LoggerFactory
import java.util.*
import kotlin.math.min

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/07 16:38
 * @project rollong-aliyun
 * @filename StsClient.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.cloud.vendor.aliyun.common.sts
 * @description
 */
class StsClient(
    private val config: StsConfig
) {

    companion object {
        private val logger = LoggerFactory.getLogger(StsClient::class.java)
    }

    private val profile: IClientProfile
        get() = DefaultProfile.getProfile(this.config.regionId, this.config.accessKey, this.config.accessSecret)

    private val client: IAcsClient
        get() = DefaultAcsClient(this.profile)

    fun assumeRule(
        expiresInSeconds: Long = 3600,
        policy: AssumeRolePolicy? = null
    ): AliyunAssumeRoleResponse {
        val roleArn = this.config.roleArn
        val roleSessionName = "${UUID.randomUUID().toString().replace("-", "")}${System.currentTimeMillis()}"
        val request = AssumeRoleRequest()
        request.sysMethod = MethodType.POST
        request.roleArn = roleArn
        request.roleSessionName = roleSessionName
        request.policy = policy?.toString() // 若policy为空，则用户将获得该角色下所有权限
        request.durationSeconds = min(1000L, expiresInSeconds) // 设置凭证有效时间
        val response: AssumeRoleResponse = this.client.getResponse(request, logger)
        return AliyunAssumeRoleResponse(
            expiration = response.credentials.expiration,
            accessKeyId = response.credentials.accessKeyId,
            accessKeySecret = response.credentials.accessKeySecret,
            securityToken = response.credentials.securityToken,
            requestId = response.requestId
        )
    }
}