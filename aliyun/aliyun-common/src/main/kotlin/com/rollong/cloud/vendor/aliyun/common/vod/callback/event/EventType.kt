package com.rollong.cloud.vendor.aliyun.common.vod.callback.event

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/07 18:54
 * @project rollong-aliyun
 * @filename EventType.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.cloud.vendor.aliyun.common.vod.callback.event
 * @description
 */
enum class EventType {
    FileUploadComplete,
    ImageUploadComplete,
    StreamTranscodeComplete,
    TranscodeComplete,
    SnapshotComplete,
    DynamicImageComplete,
    AddLiveRecordVideoComplete,
    LiveRecordVideoComposeStart,
    UploadByURLComplete,
    CreateAuditComplete,
    AIMediaAuditComplete,
    VideoAnalysisComplete,
    AIMediaDNAComplete,
    AIVideoTagComplete,
    AttachedMediaUploadComplete,
    ProduceMediaComplete,
    DeleteMediaComplete,
    MediaBaseChangeComplete,
    Unknown,
    ;

    companion object {
        fun parse(event: String?) = values().firstOrNull { it.name == event } ?: Unknown
    }
}