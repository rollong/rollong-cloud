package com.rollong.cloud.vendor.aliyun.common.sts

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/07 16:35
 * @project rollong-aliyun
 * @filename StsConfig.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.cloud.vendor.aliyun.common.sts
 * @description
 */
data class StsConfig(
    var accessKey: String = "",
    var accessSecret: String = "",
    var roleArn: String = "",
    var regionId: String = "cn-hangzhou"
)