package com.rollong.cloud.vendor.aliyun.common.rtc

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/07 13:19
 * @project rollong-aliyun
 * @filename CreateTokenResponse.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.aliyun.common.response
 * @description
 */
data class CreateTokenResponse(
    var appId: String = "",
    var userId: String = "",
    var channelId: String = "",
    var gslb: List<String> = listOf(),
    var token: String = "",
    var nonce: String = "",
    var timestamp: Long = 0
)