package com.rollong.cloud.vendor.aliyun.common.live

import com.rollong.common.util.hash.hex
import com.rollong.common.util.hash.md5
import kotlin.random.Random

class LiveDomain(
    var domain: String = "",
    var key: String = "",
    var ssl: Boolean = false
) {

    fun authKey(
        uri: String,
        uid: Long = 0,
        expiresAt: Long = System.currentTimeMillis() + 2 * 3600 * 1000L
    ): String {
        val ts = (expiresAt / 1000).toString()
        val rand = Random(System.nanoTime()).nextInt()
        val md5 = "$uri-$ts-$rand-$uid-${this.key}".md5().hex().toLowerCase()
        return "$ts-$rand-$uid-$md5"
    }

    fun pushAddress(
        appName: String,
        streamName: String,
        uid: Long = 0,
        expiresAt: Long
    ): String {
        val uri = "/$appName/$streamName"
        val authKey = this.authKey(uri, uid, expiresAt)
        return "rtmp://${this.domain}$uri?auth_key=$authKey"
    }

    fun rtmpLiveAddress(
        appName: String,
        streamName: String,
        uid: Long = 0,
        expiresAt: Long
    ): String {
        val uri = "/$appName/$streamName"
        val authKey = this.authKey(uri, uid, expiresAt)
        return "rtmp://${this.domain}$uri?auth_key=$authKey"
    }

    fun flvLiveAddress(
        appName: String,
        streamName: String,
        uid: Long = 0,
        expiresAt: Long
    ): String {
        val uri = "/$appName/$streamName.flv"
        val authKey = this.authKey(uri, uid, expiresAt)
        return "${if (this.ssl) "https" else "http"}://${this.domain}$uri?auth_key=$authKey"
    }

    fun m3u8LiveAddress(
        appName: String,
        streamName: String,
        uid: Long = 0,
        expiresAt: Long
    ): String {
        val uri = "/$appName/$streamName.m3u8"
        val authKey = this.authKey(uri, uid, expiresAt)
        return "${if (this.ssl) "https" else "http"}://${this.domain}$uri?auth_key=$authKey"
    }

    fun udpLiveAddress(
        appName: String,
        streamName: String,
        uid: Long = 0,
        expiresAt: Long
    ): String {
        val uri = "/$appName/$streamName"
        val authKey = this.authKey(uri, uid, expiresAt)
        return "artc://${this.domain}$uri?auth_key=$authKey"
    }
}