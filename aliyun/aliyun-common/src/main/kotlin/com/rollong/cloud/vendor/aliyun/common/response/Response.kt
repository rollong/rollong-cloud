package com.rollong.cloud.vendor.aliyun.common.response

import com.aliyuncs.AcsRequest
import com.aliyuncs.AcsResponse
import com.aliyuncs.IAcsClient
import com.aliyuncs.exceptions.ClientException
import com.rollong.common.exception.BaseException
import com.rollong.common.json.JSONUtil
import org.slf4j.Logger

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/07 13:51
 * @project rollong-aliyun
 * @filename Response.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.cloud.vendor.aliyun.common.util
 * @description
 */
@Throws(BaseException::class)
fun <T : AcsResponse> IAcsClient.getResponse(
    request: AcsRequest<T>,
    logger: Logger? = null,
    maxRetryCounts: Int = 0
): T = try {
    if (maxRetryCounts > 0) {
        this.getAcsResponse(request, true, maxRetryCounts)
    } else {
        this.getAcsResponse(request)
    }
} catch (e: ClientException) {
    val errorCode = "fail.aliyun.${e.errCode}"
    logger?.error(errorCode, e)
    logger?.error("requestId=${e.requestId},request=${request.javaClass.name},actionName=${request.sysActionName},requestBody=${JSONUtil.toJSONString(request)}")
    throw AliyunAPIException(
        cause = e,
        request = request,
        requestId = e.requestId
    )
}