package com.rollong.cloud.vendor.aliyun.common.rtc

import com.rollong.cloud.vendor.aliyun.common.live.LiveDomain

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/07 11:40
 * @project rollong-aliyun
 * @filename AliyunRtcConfig.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.aliyun.common.config
 * @description
 */
data class RtcConfig(
    var regionId: String = "cn-hangzhou",
    var accessKey: String = "",
    var accessSecret: String = "",
    var appId: String = "",
    var appKey: String = "",
    var gslb: String = "https://rgslb.rtc.aliyuncs.com",
    var layout: Long = 1,
    var artpPlayDomain: LiveDomain = LiveDomain(),
    var livePlayDomain: LiveDomain = LiveDomain(),
    var livePushDomain: LiveDomain = LiveDomain()
)