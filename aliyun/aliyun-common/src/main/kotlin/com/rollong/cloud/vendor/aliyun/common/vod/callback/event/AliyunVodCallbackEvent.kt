package com.rollong.cloud.vendor.aliyun.common.vod.callback.event

import com.rollong.common.json.FluentReader
import java.io.ByteArrayInputStream
import java.io.InputStream
import java.text.SimpleDateFormat
import java.util.*

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/07 17:33
 * @project rollong-aliyun
 * @filename AliyunVodCallbackEvent.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.cloud.vendor.aliyun.common.vod.callback.event
 * @description
 */
class AliyunVodCallbackEvent(inputStream: InputStream) : FluentReader(inputStream) {

    constructor(json: String) : this(ByteArrayInputStream(json.toByteArray()))

    companion object {
        private val df = SimpleDateFormat("yyyy-MM-dd HH:mm:ss").also { it.timeZone = TimeZone.getTimeZone("UTC") }
    }

    val eventType: EventType by lazy {
        EventType.parse(this.getString("EventType"))
    }

    val eventTs: Long? by lazy {
        this.getString("EventTime")?.let { df.parse(it.replace('T', ' ').replace('Z', ' ')) }?.time
    }
}