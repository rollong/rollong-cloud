package com.rollong.cloud.vendor.aliyun.common.live

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/07 14:50
 * @project rollong-aliyun
 * @filename LiveConfig.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.cloud.vendor.aliyun.common.live
 * @description
 */
class LiveConfig(
    var endpoint: String = "cn-shanghai",
    var push: LiveDomain = LiveDomain(),
    var live: LiveDomain = LiveDomain(),
    var accessKey: String = "",
    var accessSecret: String = ""
)