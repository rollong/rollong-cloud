package com.rollong.cloud.vendor.aliyun.common.sts

import com.fasterxml.jackson.annotation.JsonProperty
import com.rollong.common.json.JSONUtil

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/07 16:39
 * @project rollong-aliyun
 * @filename AssumeRolePolicy.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.cloud.vendor.aliyun.common.sts
 * @description
 */
data class AssumeRolePolicy(
    @get:JsonProperty("Version")
    var version: String = "1",
    @get:JsonProperty("Statement")
    var statement: List<Statement> = listOf()
) {
    data class Statement(
        @get:JsonProperty("Action")
        val action: List<String> = listOf(),
        @get:JsonProperty("Resource")
        var resource: List<String> = listOf(),
        @get:JsonProperty("Effect")
        var effect: Effect = Effect.Allow
    )

    enum class Effect {
        Allow, Deny
    }

    override fun toString() = JSONUtil.toJSONString(this)

    companion object {
        val ossRolePolicy = AssumeRolePolicy(
            statement = mutableListOf(Statement(action = listOf("oss:*"), resource = listOf("acs:oss:*:*:*"), effect = Effect.Allow))
        )
        val fullPolicy = AssumeRolePolicy(
            statement = mutableListOf(Statement(action = listOf("*"), resource = listOf("*"), effect = Effect.Allow))
        )
    }
}