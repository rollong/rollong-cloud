package com.rollong.cloud.vendor.aliyun.common.response

import com.aliyuncs.AcsRequest
import com.aliyuncs.exceptions.ClientException
import com.rollong.common.exception.APICallException
import com.rollong.common.exception.ExceptionConstant

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/08 15:26
 * @project rollong-cloud
 * @filename AliyunAPIException.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.cloud.vendor.aliyun.common.response
 * @description
 */
class AliyunAPIException(
    cause: ClientException,
    request: AcsRequest<*>,
    val requestId: String? = null
) : APICallException(
    errCode = "${ExceptionConstant.API_CALL}.aliyun",
    code = 500,
    message = "阿里云服务报错:${cause.errCode}:${cause.errMsg}",
    httpStatus = 500,
    api = request.javaClass.name,
    serverErrCode = cause.errCode ?: "",
    serverErrMessage = cause.errMsg ?: "",
    serverErrDescription = cause.errorDescription ?: "",
    request = request,
    response = null,
    cause = cause
)