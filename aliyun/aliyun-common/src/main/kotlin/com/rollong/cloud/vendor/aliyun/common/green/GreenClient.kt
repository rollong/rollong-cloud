package com.rollong.cloud.vendor.aliyun.common.green

import com.aliyuncs.DefaultAcsClient
import com.aliyuncs.IAcsClient
import com.aliyuncs.green.model.v20180509.ImageSyncScanRequest
import com.aliyuncs.green.model.v20180509.TextScanRequest
import com.aliyuncs.http.FormatType
import com.aliyuncs.http.MethodType
import com.aliyuncs.profile.DefaultProfile
import com.aliyuncs.profile.IClientProfile
import com.fasterxml.jackson.databind.JsonNode
import com.rollong.cloud.vendor.aliyun.common.vod.VodClient
import com.rollong.cloud.vendor.aliyun.common.vod.VodConfig
import com.rollong.common.exception.APICallException
import com.rollong.common.exception.BaseException
import com.rollong.common.json.JSONUtil
import org.slf4j.LoggerFactory
import java.nio.charset.Charset
import java.util.*

/**
 * Title：GreenClient
 * Description：
 * @author Flicker
 * @create 2021/3/16 11:24
 **/
class GreenClient(
    private val config: GreenConfig
) {
    companion object {
        private val logger = LoggerFactory.getLogger(GreenClient::class.java)
    }

    private val profile: IClientProfile
        get() {
            val profile = DefaultProfile.getProfile(this.config.regionId, this.config.accessKey, this.config.accessSecret)
            DefaultProfile.addEndpoint(
                "cn-shanghai", "Green", "green.cn-shanghai.aliyuncs.com"
            )
            return profile
        }

    private val client: IAcsClient
        get() = DefaultAcsClient(this.profile)

    fun textScan(texts: Collection<String>, bizType: String? = null): Iterable<JsonNode>? {
        val textScanRequest = TextScanRequest().also {
            it.sysAcceptFormat = FormatType.JSON
            it.httpContentType = FormatType.JSON
            it.sysMethod = MethodType.POST
            it.sysEncoding = "UTF-8"
            it.sysRegionId = "cn-shanghai"
            it.sysConnectTimeout = 3000
            it.sysReadTimeout = 6000
        }
        val tasks = texts.map {
            linkedMapOf(
                "dataId" to UUID.randomUUID().toString(),
                "content" to it
            )
        }
        val data = mapOf(
            "tasks" to tasks,
            "bizType" to bizType,
            "scenes" to listOf("antispam")
        )
        textScanRequest.setHttpContent(
            JSONUtil.toJSONString(data).toByteArray(Charset.forName("UTF-8")),
            "UTF-8",
            FormatType.JSON
        )
        val httpResponse = client.doAction(textScanRequest)
        if (!httpResponse.isSuccess) {
            throw BaseException("response not success. status:" + httpResponse.status)
        }
        val scrResponseJson = String(httpResponse.httpContent, Charset.forName("UTF-8"))
        logger.debug("aliyun green text response:${scrResponseJson}")
        val scrResponse = JSONUtil.objectMapper.readTree(scrResponseJson)
        val code = scrResponse.get("code").asInt()
        var pass = true
        if (200 == code) {
            val taskResults = scrResponse.get("data").asIterable()
            for (taskResult in taskResults) {
                val results = taskResult.get("results").asIterable()
                val block = results.map { it.get("suggestion").asText() }.filter { it == "block" }
                if (block.isNotEmpty()) {
                    pass = false
                    break
                }
            }
            return if (!pass) {
                taskResults
            } else {
                null
            }
        } else {
            throw BaseException("detect not success. code:$code,payload:${scrResponseJson}")
        }
    }

    fun imageSyncScan(urls: List<String>, bizType: String = "polar"): Iterable<JsonNode>? {
        val request = ImageSyncScanRequest().also {
            it.sysAcceptFormat = FormatType.JSON
            it.sysMethod = MethodType.POST
            it.sysEncoding = "UTF-8"
            it.sysRegionId = "cn-shanghai"
            it.sysConnectTimeout = 3000
            it.sysReadTimeout = 6000
        }
        val tasks = urls.map {
            linkedMapOf(
                "dataId" to UUID.randomUUID().toString(),
                "time" to Date(),
                "url" to it
            )
        }
        val data = mapOf(
            "tasks" to tasks,
            "bizType" to bizType,
            "scenes" to listOf("porn")
        )
        request.setHttpContent(
            JSONUtil.toJSONString(data).toByteArray(Charset.forName("UTF-8")),
            "UTF-8",
            FormatType.JSON
        )
        val httpResponse = client.doAction(request)
        if (httpResponse == null || !httpResponse.isSuccess) {
            throw APICallException(message = "response not success. status:" + httpResponse.status)
        }
        val scrResponseJson = String(httpResponse.httpContent, Charset.forName("UTF-8"))
        logger.debug("aliyun green image response:${scrResponseJson}")
        val scrResponse = JSONUtil.objectMapper.readTree(scrResponseJson)
        val code = scrResponse.get("code").asInt()
        var pass = true
        if (200 == code) {
            val taskResults = scrResponse.get("data").asIterable()
            for (taskResult in taskResults) {
                val results = taskResult.get("results").asIterable()
                val block = results.map { it.get("suggestion").asText() }.filter { it == "block" }
                if (block.isNotEmpty()) {
                    pass = false
                    break
                }
            }
            return if (!pass) {
                taskResults
            } else {
                null
            }
        } else {
            throw APICallException(message = "detect not success. code:$code,payload:${scrResponseJson}")
        }
    }
}