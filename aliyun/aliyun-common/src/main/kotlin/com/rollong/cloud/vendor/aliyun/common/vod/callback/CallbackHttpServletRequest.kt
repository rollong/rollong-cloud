package com.rollong.cloud.vendor.aliyun.common.vod.callback

import com.rollong.cloud.vendor.aliyun.common.vod.callback.event.AliyunVodCallbackEvent
import javax.servlet.http.HttpServletRequest

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/07 19:32
 * @project rollong-aliyun
 * @filename CallbackHttpServletRequest.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.cloud.vendor.aliyun.common.vod.callback
 * @description
 */
fun HttpServletRequest.toVodCallbackEvent(
    callbackURL: String? = null,
    privateKeys: Collection<String>? = null
): AliyunVodCallbackEvent {
    var timestamp: Int? = null
    var signature: String? = null
    var url: String? = null
    if (null != privateKeys && privateKeys.isNotEmpty()) {
        timestamp = this.getIntHeader("X-VOD-TIMESTAMP")
        signature = this.getHeader("X-VOD-SIGNATURE")
        url = callbackURL ?: this.requestURL.toString()
    }
    return VodCallbackHandler.onCallback(
        timestamp, signature, url, privateKeys, this.inputStream
    )
}