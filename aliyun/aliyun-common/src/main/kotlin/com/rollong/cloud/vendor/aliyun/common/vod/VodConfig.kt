package com.rollong.cloud.vendor.aliyun.common.vod

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/07 16:53
 * @project rollong-aliyun
 * @filename VodConfig.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.cloud.vendor.aliyun.common.vod
 * @description
 */
class VodConfig(
    var regionId: String = "cn-shanghai",
    var cateId: Long = 0L,
    var workflowId: String = "",
    var accessKey: String = "",
    var accessSecret: String = "",
    var callbackPrivateKeys: List<String>? = null
)