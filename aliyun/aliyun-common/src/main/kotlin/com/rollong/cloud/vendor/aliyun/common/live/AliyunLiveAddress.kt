package com.rollong.cloud.vendor.aliyun.common.live

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/07 14:32
 * @project rollong-aliyun
 * @filename AliyunLiveAddress.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.cloud.vendor.aliyun.common.live
 * @description
 */
data class AliyunLiveAddress(
    var appName: String = "",
    var streamName: String = "",
    var expiresAt: Long = 0,
    var rtmp: String = "",
    var flv: String = "",
    var m3u8: String = "",
    var artc: String = ""
)