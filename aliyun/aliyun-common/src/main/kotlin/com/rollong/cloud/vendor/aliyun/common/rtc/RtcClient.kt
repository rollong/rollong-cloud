package com.rollong.cloud.vendor.aliyun.common.rtc

import com.aliyuncs.AcsRequest
import com.aliyuncs.AcsResponse
import com.aliyuncs.DefaultAcsClient
import com.aliyuncs.IAcsClient
import com.aliyuncs.profile.DefaultProfile
import com.aliyuncs.profile.IClientProfile
import com.aliyuncs.rtc.model.v20180111.*
import com.rollong.cloud.vendor.aliyun.common.response.getResponse
import com.rollong.common.exception.BaseException
import org.slf4j.LoggerFactory
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.util.*
import javax.xml.bind.DatatypeConverter

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/07 11:04
 * @project rollong-aliyun
 * @filename RtcClient.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.rtc.ali.server
 * @description https://help.aliyun.com/document_detail/71773.html?spm=a2c4g.11174283.6.696.61623c3dfUN0Be
 */
class RtcClient(
    private val config: RtcConfig
) {

    companion object {
        private val logger = LoggerFactory.getLogger(RtcClient::class.java)
    }

    private val profile: IClientProfile
        get() = DefaultProfile.getProfile(this.config.regionId, this.config.accessKey, this.config.accessSecret)

    private val client: IAcsClient
        get() = DefaultAcsClient(this.profile)

    @Throws(BaseException::class)
    fun <T : AcsResponse> call(request: AcsRequest<T>) = this.client.getResponse(request, logger)

    /**
     * 调用DeleteChannel删除频道。
     */
    @Throws(BaseException::class)
    fun deleteChannel(appId: String, channelId: String): DeleteChannelResponse {
        val request = DeleteChannelRequest()
        request.appId = appId
        request.channelId = channelId
        return this.client.getResponse(request, logger)
    }

    /**
     * 调用DescribeUserInfoInChannel查询用户在频道中实时信息。
     */
    @Throws(BaseException::class)
    fun describeUserInfoInChannel(appId: String, channelId: String, userId: String): DescribeUserInfoInChannelResponse {
        val request = DescribeUserInfoInChannelRequest()
        request.appId = appId
        request.channelId = channelId
        request.userId = userId
        return this.client.getResponse(request, logger)
    }

    /**
     * 调用DescribeChannelUsers查询Channel（频道）实时在线用户列表。
     */
    @Throws(BaseException::class)
    fun describeChannelUsers(appId: String, channelId: String): DescribeChannelUsersResponse {
        val request = DescribeChannelUsersRequest()
        request.appId = appId
        request.channelId = channelId
        return this.client.getResponse(request, logger)
    }

    @Throws(BaseException::class)
    fun startMPUTask(channelId: String, pushUrl: String, taskId: String, appId: String, userId: String): StartMPUTaskResponse {
        val request = StartMPUTaskRequest()
        request.appId = appId
        request.channelId = channelId
        request.layoutIdss = listOf(this.config.layout)
        request.mediaEncode = 30
        request.taskProfile = "9IN_1080P"
        request.streamURL = pushUrl
        request.taskId = taskId
        val userPanesList: MutableList<StartMPUTaskRequest.UserPanes> = ArrayList()
        val userPanes = StartMPUTaskRequest.UserPanes()
        userPanes.paneId = 0
        userPanes.sourceType = "camera"
        userPanes.userId = userId
        userPanesList.add(userPanes)
        request.userPaness = userPanesList
        return this.client.getResponse(request, logger)
    }

    @Throws(BaseException::class)
    fun updateMPULayout(request: UpdateMPULayoutRequest): UpdateMPULayoutResponse {
        return this.client.getResponse(request, logger)
    }


    @Throws(BaseException::class)
    fun stopMPUTaskRequest(request: StopMPUTaskRequest): StopMPUTaskResponse {
        return this.client.getResponse(request, logger)
    }

    /**
     * 计算直播域名的访问token
     */
    @Throws(NoSuchAlgorithmException::class)
    fun createToken(
        channelId: String,
        userId: String,
        expiresAt: Long = System.currentTimeMillis() + 2 * 3600 * 1000L
    ): CreateTokenResponse {
        val appId: String = this.config.appId
        val appKey: String = this.config.appKey
        val gslb: String = this.config.gslb
        val nonce: String = String.format("AK-%s", UUID.randomUUID().toString())
        val timestamp = expiresAt / 1000
        val digest = MessageDigest.getInstance("SHA-256")
        digest.update(appId.toByteArray())
        digest.update(appKey.toByteArray())
        digest.update(channelId.toByteArray())
        digest.update(userId.toByteArray())
        digest.update(nonce.toByteArray())
        digest.update(timestamp.toString().toByteArray())
        val rtcToken = DatatypeConverter.printHexBinary(digest.digest()).toLowerCase()
        return CreateTokenResponse(appId, userId, channelId, listOf(gslb), rtcToken, nonce, timestamp)
    }

    @Throws(BaseException::class)
    fun describeRTCAppKey(appId: String): DescribeRTCAppKeyResponse {
        val request = DescribeRTCAppKeyRequest()
        request.appId = appId
        return this.client.getResponse(request, logger)
    }

}