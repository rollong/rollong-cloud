package com.rollong.cloud.vendor.aliyun.common.green

/**
 * Title：GreenConfig
 * Description：
 * @author Flicker
 * @create 2021/3/16 11:23
 **/
class GreenConfig {
    var regionId: String = "cn-shanghai"
    var accessKey: String = ""
    var accessSecret: String = ""
    var bizType: String = ""
}