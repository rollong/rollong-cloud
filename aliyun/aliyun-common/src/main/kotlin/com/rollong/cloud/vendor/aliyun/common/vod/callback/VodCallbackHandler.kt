package com.rollong.cloud.vendor.aliyun.common.vod.callback

import com.rollong.cloud.vendor.aliyun.common.vod.callback.event.AliyunVodCallbackEvent
import com.rollong.common.exception.BadRequestException
import com.rollong.common.json.JSONUtil
import com.rollong.common.util.hash.hex
import com.rollong.common.util.hash.md5
import org.slf4j.LoggerFactory
import java.io.InputStream

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/07 17:17
 * @project rollong-aliyun
 * @filename VodCallbackHandler.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.cloud.vendor.aliyun.common.vod
 * @description
 */
object VodCallbackHandler {

    private val logger = LoggerFactory.getLogger(VodCallbackHandler::class.java)

    fun onCallback(
        timestamp: Int? = null,
        signature: String? = null,
        callbackURL: String? = null,
        privateKeys: Collection<String>? = null,
        inputStream: InputStream
    ): AliyunVodCallbackEvent {
        privateKeys?.let {
            if (it.isNotEmpty()) {
                if (null != signature && null != callbackURL && null != timestamp) {
                    var passed = false
                    for (key in it) {
                        val sign = "$callbackURL|$timestamp|$key".md5().hex().toLowerCase()
                        if (sign == signature.toLowerCase()) {
                            passed = true
                            break
                        }
                    }
                    if (!passed) {
                        logger.error(
                            "aliyun.vod.callback认证失败,request={}",
                            JSONUtil.toJSONString(mapOf("timestamp" to timestamp, "signature" to signature, "callbackURL" to callbackURL))
                        )
                        throw BadRequestException("callback认证失败")
                    }
                } else {
                    logger.error("aliyun.vod.callback需要鉴权参数")
                    throw BadRequestException("callback需要鉴权参数")
                }
            }
        }
        return AliyunVodCallbackEvent(inputStream)
    }
}