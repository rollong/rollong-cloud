package com.rollong.cloud.vendor.aliyun.common.vod

import com.aliyuncs.DefaultAcsClient
import com.aliyuncs.IAcsClient
import com.aliyuncs.profile.DefaultProfile
import com.aliyuncs.profile.IClientProfile
import com.aliyuncs.vod.model.v20170321.*
import com.rollong.cloud.vendor.aliyun.common.response.getResponse
import com.rollong.common.exception.BaseException
import com.rollong.common.json.JSONUtil
import com.rollong.common.util.collection.implode
import org.slf4j.LoggerFactory
import java.net.URLEncoder

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/07 16:52
 * @project rollong-aliyun
 * @filename VodClient.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.cloud.vendor.aliyun.common.vod
 * @description
 */
class VodClient(
    private val config: VodConfig
) {

    companion object {
        private val logger = LoggerFactory.getLogger(VodClient::class.java)
    }

    private val profile: IClientProfile
        get() = DefaultProfile.getProfile(this.config.regionId, this.config.accessKey, this.config.accessSecret)

    private val client: IAcsClient
        get() = DefaultAcsClient(this.profile)

    @Throws(BaseException::class)
    fun createUploadVideo(
        title: String,
        filename: String,
        tags: List<String>? = null,
        callbackURL: String? = null,
        extend: Any? = null,
        workflowId: String? = null,
        cateId: Long? = null
    ): CreateUploadVideoResponse {
        val request = CreateUploadVideoRequest()
        request.title = title
        request.fileName = filename
        tags?.let { request.tags = it.implode(",") }
        //UserData，用户自定义设置参数，用户需要单独回调URL及数据透传时设置(非必须)
        val userdata = mutableMapOf<String, Any>()
        //UserData回调部分设置
        if (null != callbackURL) {
            val messageCallback = mapOf(
                "CallbackURL" to callbackURL,
                "CallbackType" to "http"
            )
            userdata["MessageCallback"] = JSONUtil.toJSONString(messageCallback)
        }
        //UserData透传数据部分设置
        if (null != extend) {
            userdata["Extend"] = JSONUtil.toJSONString(extend)
        }
        request.userData = JSONUtil.toJSONString(userdata)
        request.workflowId = workflowId ?: this.config.workflowId
        request.cateId = cateId ?: this.config.cateId
        return this.client.getResponse(request, logger)
    }

    /**
     * URL批量拉取上传
     * uploadUrls: URL编码，多个地址以英文逗号（,）分隔，最多支持20个
     */
    fun uploadMediaByURL(uploadUrls: String,
                         title: String,
                         tags: List<String>? = null,
                         callbackURL: String? = null,
                         extend: Any? = null,
                         workflowId: String? = null,
                         cateId: Long? = null
    ): UploadMediaByURLResponse {
        val request = UploadMediaByURLRequest()
        val encodeUrl = URLEncoder.encode(uploadUrls, "UTF-8")
        request.uploadURLs = encodeUrl
        val uploadMetadata = mutableMapOf<String, Any?>()
        uploadMetadata["SourceUrl"] = encodeUrl
        uploadMetadata["Title"] = title
        uploadMetadata["CateId"] = cateId ?: this.config.cateId
        tags?.let { uploadMetadata["Tags"] = it.joinToString(",") }
        val uploadMetadataList = mutableListOf<MutableMap<String, Any?>>()
        uploadMetadataList.add(uploadMetadata)
        request.uploadMetadatas = JSONUtil.toJSONString(uploadMetadataList)

        val userData = mutableMapOf<String, Any?>()
        callbackURL?.let {
            val messageCallback = mutableMapOf<String, Any?>()
            messageCallback["CallbackURL"] = callbackURL
            messageCallback["CallbackType"] = "http"
            userData["MessageCallback"] = JSONUtil.toJSONString(messageCallback)
        }
        extend?.let {
            userData["Extend"] = JSONUtil.toJSONString(extend)
        }
        request.userData = JSONUtil.toJSONString(userData)
        request.workflowId = workflowId ?: this.config.workflowId
        return this.client.getResponse(request, logger)
    }

    /**
     * 刷新视频上传凭证
     * @param client 发送请求客户端
     * @return RefreshUploadVideoResponse 刷新视频上传凭证响应数据
     * @throws Exception
     */
    @Throws(BaseException::class)
    fun refreshUploadVideo(videoId: String): RefreshUploadVideoResponse {
        val request = RefreshUploadVideoRequest()
        request.videoId = videoId
        return this.client.getResponse(request, logger)
    }

    /**
     * 获取播放地址
     * @return RefreshUploadVideoResponse 刷新视频上传凭证响应数据
     * @throws Exception
     */
    @Throws(BaseException::class)
    fun getPlayInfo(videoId: String): GetPlayInfoResponse {
        val request = GetPlayInfoRequest()
        request.videoId = videoId
        return this.client.getResponse(request, logger, 10)
    }

    @Throws(BaseException::class)
    fun getMediaAuditResult(videoId: String): GetMediaAuditResultResponse {
        val request = GetMediaAuditResultRequest()
        request.mediaId = videoId
        return this.client.getResponse(request, logger, 10)
    }

    @Throws(BaseException::class)
    fun createAudit(auditContent: List<Any>): CreateAuditResponse {
        val request = CreateAuditRequest()
        request.auditContent = JSONUtil.toJSONString(auditContent)
        return this.client.getResponse(request, logger, 10)
    }


}