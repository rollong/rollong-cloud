package com.rollong.cloud.vendor.aliyun.common.live

import com.aliyuncs.DefaultAcsClient
import com.aliyuncs.IAcsClient
import com.aliyuncs.live.model.v20161101.DescribeLiveStreamsOnlineListRequest
import com.aliyuncs.live.model.v20161101.DescribeLiveStreamsOnlineListResponse
import com.aliyuncs.profile.DefaultProfile
import com.aliyuncs.profile.IClientProfile
import com.rollong.cloud.vendor.aliyun.common.response.getResponse
import com.rollong.common.exception.BaseException
import org.slf4j.LoggerFactory

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/07 14:34
 * @project rollong-aliyun
 * @filename LiveClient.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.rtc.ali.server.live
 * @description
 */
class LiveClient(
    private val config: LiveConfig
) {

    companion object {
        private val logger = LoggerFactory.getLogger(LiveClient::class.java)
    }

    private val profile: IClientProfile
        get() = DefaultProfile.getProfile(this.config.endpoint, this.config.accessKey, this.config.accessSecret)

    private val client: IAcsClient
        get() = DefaultAcsClient(this.profile)

    fun getPushAddress(
        appName: String,
        streamName: String,
        uid: Long = 0,
        expiresAt: Long
    ) = AliyunLivePushAddress(
        appName = appName,
        streamName = streamName,
        address = this.config.push.pushAddress(
            appName, streamName, uid, expiresAt
        ),
        expiresAt = expiresAt
    )

    fun getLiveAddress(
        appName: String,
        streamName: String,
        uid: Long = 0,
        expiresAt: Long
    ) = AliyunLiveAddress(
        appName = appName,
        streamName = streamName,
        expiresAt = expiresAt,
        rtmp = this.config.live.rtmpLiveAddress(appName, streamName, uid, expiresAt),
        flv = this.config.live.flvLiveAddress(appName, streamName, uid, expiresAt),
        m3u8 = this.config.live.m3u8LiveAddress(appName, streamName, uid, expiresAt),
        artc = this.config.live.udpLiveAddress(appName, streamName, uid, expiresAt)
    )

    /**
     * https://help.aliyun.com/document_detail/35409.html?spm=a2c4g.11186623.6.771.3ae9234cl6DcR0
     */
    @Throws(BaseException::class)
    fun describeLiveStreamsOnlineList(
        appName: String? = null,
        streamName: String? = null,
        page: Int = 0,
        size: Int = 20
    ): DescribeLiveStreamsOnlineListResponse {
        val request = DescribeLiveStreamsOnlineListRequest()
        request.appName = appName
        request.streamName = streamName
        request.domainName = this.config.push.domain
        request.pageNum = page + 1
        request.pageSize = size
        return this.client.getResponse(request, logger)
    }

}