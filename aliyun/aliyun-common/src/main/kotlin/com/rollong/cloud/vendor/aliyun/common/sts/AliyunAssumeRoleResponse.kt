package com.rollong.cloud.vendor.aliyun.common.sts

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/07 16:36
 * @project rollong-aliyun
 * @filename AliyunAssumeRoleResponse.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.cloud.vendor.aliyun.common.sts
 * @description
 */
data class AliyunAssumeRoleResponse(
    var expiration: String? = null,
    var accessKeyId: String? = null,
    var accessKeySecret: String? = null,
    var securityToken: String? = null,
    var requestId: String? = null
)