package com.rollong.cloud.vendor.aliyun.common.live

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/07 14:33
 * @project rollong-aliyun
 * @filename AliyunLivePushAddress.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.cloud.vendor.aliyun.common.live
 * @description
 */
data class AliyunLivePushAddress(
    var appName: String = "",
    var streamName: String = "",
    var address: String = "",
    var expiresAt: Long = 0
)