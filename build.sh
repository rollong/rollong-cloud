#!/bin/bash

./gradlew clean :aliyun:install :tencent:install :aliyun:aliyun-common:clean :aliyun:aliyun-common:build :aliyun:aliyun-server:clean :aliyun:aliyun-server:build \
  :tencent:tencent-common:clean :tencent:tencent-common:build \
  -x test
