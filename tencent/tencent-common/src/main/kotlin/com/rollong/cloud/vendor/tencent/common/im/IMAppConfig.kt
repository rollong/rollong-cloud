package com.rollong.cloud.vendor.tencent.common.im

import com.rollong.cloud.vendor.tencent.common.config.AppConfig

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/08 17:16
 * @project rollong-cloud
 * @filename IMAppConfig.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.cloud.vendor.tencent.common.im
 * @description
 */
data class IMAppConfig(
    /**
     * 账号管理员userId
     * @reference https://cloud.tencent.com/document/product/269/32578#.E9.85.8D.E7.BD.AE.E5.B8.90.E5.8F.B7.E7.AE.A1.E7.90.86.E5.91.98
     */
    var administrator: String = "administrator"
) : AppConfig()