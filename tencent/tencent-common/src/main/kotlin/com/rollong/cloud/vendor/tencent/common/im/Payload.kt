package com.rollong.cloud.vendor.tencent.common.im

import com.fasterxml.jackson.annotation.JsonProperty
import com.rollong.cloud.vendor.tencent.common.response.TencentCloudAPIException

class Payload {

    open class Response {
        @JsonProperty("ActionStatus")
        open var actionStatus: String? = null
        @JsonProperty("ErrorInfo")
        open var errorInfo: String? = null
        @JsonProperty("ErrorCode")
        open var errorCode: Int? = null

        @Throws(TencentCloudAPIException::class)
        fun check() {
            if (0 != this.errorCode) {
                throw TencentCloudAPIException().also {
                    it.serverErrCode = this.errorCode?.toString()
                    it.serverErrMessage = this.errorInfo
                    it.message = "腾讯IM接口返回错误,${this.actionStatus}/${this.errorCode}:${this.errorInfo}"
                }
            }
        }
    }

    //https://cloud.tencent.com/document/product/269/1615
    data class CreateGroupRequest(
        @JsonProperty("Owner_Account")
        var ownerAccount: String? = null,
        @JsonProperty("Type")
        var type: GroupType?,
        @JsonProperty("GroupId")
        var groupId: String = "",
        @JsonProperty("Name")
        var name: String = "",
        @JsonProperty("ApplyJoinOption")
        var applyJoinOption: String = "FreeAccess",
        @JsonProperty("MemberList")
        var MemberList: List<CreateGroupSetMemberRequest>? = null
    ) {
        enum class GroupType{
            Private,
            Public,
            ChatRoom,
            AVChatRoom
        }
    }

    data class CreateGroupSetMemberRequest(
        @JsonProperty("Member_Account")
        var memberAccount: String = "",
        @JsonProperty("Role")
        var role: String? = null
    )

    data class CreateGroupResponse(
        @JsonProperty("GroupId")
        var groupId: String? = null
    ) : Response()

    data class DestroyGroupRequest(
        @JsonProperty("GroupId")
        var groupId: String = ""
    )

    data class GetGroupInfoRequest(
        @JsonProperty("GroupIdList")
        var groupIdList: List<String>
    )

    data class GetGroupInfoResponse(
        var GroupInfo: List<GroupInfoItem> = listOf()
    ) : Response() {
        data class GroupInfoItem(
            @JsonProperty("GroupId")
            var groupId: String? = null,
            @JsonProperty("Type")
            var type: String? = null,
            @JsonProperty("Name")
            var name: String? = null
        ) : Response()
    }

    data class SendGroupMsgRequest(
        @JsonProperty("GroupId")
        var groupId: String,
        @JsonProperty("Random")
        var random: String,
        // 消息的优先级，默认为Normal
        @JsonProperty("MsgPriority")
        var msgPriority: MsgPriority = MsgPriority.Normal,
        // 消息体
        @JsonProperty("MsgBody")
        var msgBody: List<MessageBody>,
        // 消息来源帐号，选填。如果不填写该字段，则默认消息的发送者为调用该接口时使用的 App 管理员帐号
        @JsonProperty("From_Account")
        var fromAccount: String? = null,

        @JsonProperty("OnlineOnlyFlag")
        var OnlineOnlyFlag: Int,
        @JsonProperty("SendMsgControl")
        var SendMsgControl: Int,
        @JsonProperty("CloudCustomData")
        var CloudCustomData: Int,
    ) {
        data class MessageBody(
            @JsonProperty("MsgType")
            var msgType: MsgType,
            @JsonProperty("MsgContent")
            var msgContent: MsgContent,
        )
        data class MsgContent(
            @JsonProperty("Text")
            var text: String? = null,
            @JsonProperty("Desc")
            var desc: String? = null,
            @JsonProperty("Latitude")
            var latitude: String? = null,
            @JsonProperty("Longitude")
            var longitude: String? = null,
            @JsonProperty("Index")
            var index: Int? = null,
            @JsonProperty("Data")
            var data: String? = null,
            @JsonProperty("Ext")
            var ext: String? = null,
            @JsonProperty("Sound")
            var sound: String? = null,
            @JsonProperty("Url")
            var url: String? = null,
            @JsonProperty("Size")
            var size: Long? = null,
            @JsonProperty("Second")
            var second: Int? = null,
            @JsonProperty("Download_Flag")
            var downloadFlag: Int? = null,
            @JsonProperty("UUID")
            var uuid: String? = null,
            @JsonProperty("ImageFormat")
            var imageFormat: Int? = null,
            @JsonProperty("ImageInfoArray")
            var imageInfoArray: List<ImageInfoArray>? = null,
            @JsonProperty("FileSize")
            var fileSize: Long? = null,
            @JsonProperty("FileName")
            var fileName: String? = null,
            @JsonProperty("VideoUrl")
            var videoUrl: String? = null,
            @JsonProperty("VideoSize")
            var videoSize: Long? = null,
            @JsonProperty("VideoSecond")
            var videoSecond: Int? = null,
            @JsonProperty("VideoFormat")
            var videoFormat: String? = null,
            @JsonProperty("VideoDownloadFlag")
            var videoDownloadFlag: Int? = null,
            @JsonProperty("ThumbUrl")
            var thumbUrl: String? = null,
            @JsonProperty("ThumbSize")
            var thumbSize: Long? = null,
            @JsonProperty("ThumbWidth")
            var thumbWidth: Int? = null,
            @JsonProperty("ThumbHeight")
            var thumbHeight: Int? = null,
            @JsonProperty("ThumbFormat")
            var thumbFormat: String? = null,
            @JsonProperty("ThumbDownloadFlag")
            var thumbDownloadFlag: Int? = null,
        )
        data class ImageInfoArray(
            @JsonProperty("Type")
            var type: Int? = null,
            @JsonProperty("Size")
            var size: Long? = null,
            @JsonProperty("Width")
            var width: Int? = null,
            @JsonProperty("Height")
            var height: Int? = null,
            @JsonProperty("URL")
            var url: String? = null,
        )
        enum class MsgPriority{
            High, Normal, Low
        }
        enum class MsgType(var value: String){
            TIMTextElem("文本消息"),
            TIMLocationElem("位置消息"),
            TIMFaceElem("表情消息"),
            TIMCustomElem("自定义消息"),
            TIMSoundElem("语音消息"),
            TIMImageElem("图像消息"),
            TIMFileElem("文件消息"),
            TIMVideoFileElem("视频消息")
        }
    }

    data class SendGroupMsgResponse(
        @JsonProperty("MsgTime")
        var msgTime: Long? = null,
        @JsonProperty("MsgSeq")
        var msgSeq: Long? = null,
    ) : Response()
}