package com.rollong.cloud.vendor.tencent.common.usersig

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/08 14:47
 * @project rollong-cloud
 * @filename UserSig.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.cloud.vendor.tencent.common.rtc
 * @description
 */
data class UserSig(
    val appId: Long,
    val userId: String,
    val userSig: String,
    val expiresAt: Long
)