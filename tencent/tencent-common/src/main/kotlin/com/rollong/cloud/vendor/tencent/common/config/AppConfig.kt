package com.rollong.cloud.vendor.tencent.common.config

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/08 16:24
 * @project rollong-cloud
 * @filename AppConfig.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.cloud.vendor.tencent.common.config
 * @description
 */
open class AppConfig(
    var appId: Long = 0L,
    var appKey: String = "",
    var expireInSeconds: Long = 60
)