package com.rollong.cloud.vendor.tencent.common.im.feignclient

import com.rollong.cloud.vendor.tencent.common.im.Payload
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam

/**
 * api doc url: https://cloud.tencent.com/document/product/269/1619
 */
@FeignClient(name = "tencent-cloud-im", url = "https://console.tim.qq.com/")
interface TencentCloudImAPI {

    /**
     * 创建群组
     */
    @PostMapping("v4/group_open_http_svc/create_group",
        consumes = [MediaType.APPLICATION_JSON_VALUE], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun createGroup(
        @RequestParam("sdkappid") sdkappid: Long,
        @RequestParam("identifier") identifier: String,
        @RequestParam("usersig") usersig: String,
        @RequestParam("random") random: Long = System.currentTimeMillis() / 1000,
        @RequestParam("contenttype") contenttype: String = "json",
        @RequestBody request: Payload.CreateGroupRequest
    ): Payload.CreateGroupResponse

    /**
     * 解散群组
     */
    @PostMapping("v4/group_open_http_svc/destroy_group",
        consumes = [MediaType.APPLICATION_JSON_VALUE], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun destroyGroup(
        @RequestParam("sdkappid") sdkappid: Long,
        @RequestParam("identifier") identifier: String,
        @RequestParam("usersig") usersig: String,
        @RequestParam("random") random: Long = System.currentTimeMillis() / 1000,
        @RequestParam("contenttype") contenttype: String = "json",
        @RequestBody request: Payload.DestroyGroupRequest
    ): Payload.CreateGroupResponse

    /**
     * 获取群详细资料
     */
    @PostMapping("v4/group_open_http_svc/get_group_info",
        consumes = [MediaType.APPLICATION_JSON_VALUE], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getGroupInfo(
        @RequestParam("sdkappid") sdkappid: Long,
        @RequestParam("identifier") identifier: String,
        @RequestParam("usersig") usersig: String,
        @RequestParam("random") random: Long = System.currentTimeMillis() / 1000,
        @RequestParam("contenttype") contenttype: String = "json",
        @RequestBody request: Payload.GetGroupInfoRequest
    ): Payload.GetGroupInfoResponse

    /**
     * 在群组中发送普通消息
     * https://cloud.tencent.com/document/product/269/1629
     */
    @PostMapping("v4/group_open_http_svc/send_group_msg",
        consumes = [MediaType.APPLICATION_JSON_VALUE], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun sendGroupMsg(
        @RequestParam("sdkappid") sdkappid: Long,
        @RequestParam("identifier") identifier: String,
        @RequestParam("usersig") usersig: String,
        @RequestParam("random") random: Long = System.currentTimeMillis() / 1000,
        @RequestParam("contenttype") contenttype: String = "json",
        @RequestBody request: Payload.SendGroupMsgRequest
    ): Payload.SendGroupMsgResponse
}