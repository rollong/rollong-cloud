package com.rollong.cloud.vendor.tencent.common.im

import com.rollong.cloud.vendor.tencent.common.im.feignclient.TencentCloudImAPI
import com.rollong.cloud.vendor.tencent.common.response.TencentCloudAPIException
import com.rollong.cloud.vendor.tencent.common.usersig.UserSig
import com.rollong.cloud.vendor.tencent.common.usersig.UserSignGenerator

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/08 16:05
 * @project rollong-cloud
 * @filename IMClient.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.cloud.vendor.tencent.common.im
 * @description
 */
class IMClient(
    private val appConfig: IMAppConfig,
    private val api: TencentCloudImAPI
) {

    private fun administratorSign(): UserSig {
        return UserSignGenerator.generate(
            userId = this.appConfig.administrator,
            appId = this.appConfig.appId,
            appKey = this.appConfig.appKey,
            expireInSeconds = this.appConfig.expireInSeconds
        )
    }
    
    fun createGroup(
        id: String,
        name: String,
        type: Payload.CreateGroupRequest.GroupType = Payload.CreateGroupRequest.GroupType.ChatRoom
    ): Payload.CreateGroupResponse {
        val sign = this.administratorSign()
        val resp = this.api.createGroup(
            sdkappid = this.appConfig.appId,
            identifier = this.appConfig.administrator,
            usersig = sign.userSig,
            request = Payload.CreateGroupRequest(
                type = type,
                groupId = id,
                name = name
            )
        )
        resp.check()
        return resp
    }

    fun getOrCreateGroup(id: String, name: String, type: Payload.CreateGroupRequest.GroupType = Payload.CreateGroupRequest.GroupType.ChatRoom): Payload.GetGroupInfoResponse.GroupInfoItem? {
        return try {
            this.getGroup(id = id)
        } catch (e: TencentCloudAPIException) {
            if (e.errCode == "10010" || e.errCode == "10015") {
                this.createGroup(id = id, name = name, type = type)
                this.getGroup(id = id)
            } else {
                throw e
            }
        }
    }

    fun getGroup(id: String): Payload.GetGroupInfoResponse.GroupInfoItem? {
        val sign = this.administratorSign()
        val resp = this.api.getGroupInfo(
            sdkappid = this.appConfig.appId,
            identifier = this.appConfig.administrator,
            usersig = sign.userSig,
            request = Payload.GetGroupInfoRequest(
                groupIdList = listOf(id)
            )
        )
        resp.check()
        return resp.GroupInfo.firstOrNull { it.groupId == id }?.let { it.check();it }
    }

    fun destroyGroup(id: String): Payload.Response {
        val sign = this.administratorSign()
        val resp = this.api.destroyGroup(
            sdkappid = this.appConfig.appId,
            identifier = this.appConfig.administrator,
            usersig = sign.userSig,
            request = Payload.DestroyGroupRequest(
                groupId = id
            )
        )
        resp.check()
        return resp
    }

    fun sendGroupMsg(request: Payload.SendGroupMsgRequest): Payload.SendGroupMsgResponse {
        val sign = this.administratorSign()
        val resp = this.api.sendGroupMsg(
            sdkappid = this.appConfig.appId,
            identifier = this.appConfig.administrator,
            usersig = sign.userSig,
            request = request
        )
        resp.check()
        return resp
    }
}