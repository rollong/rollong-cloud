package com.rollong.cloud.vendor.tencent.common.response

import com.rollong.common.exception.APICallException
import com.rollong.common.exception.ExceptionConstant
import com.tencentcloudapi.common.exception.TencentCloudSDKException

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/08 16:02
 * @project rollong-cloud
 * @filename TencentCloudAPIException.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.cloud.vendor.tencent.common.response
 * @description
 */
class TencentCloudAPIException(
    val requestId: String? = null,
    cause: Throwable? = null
) : APICallException(
    errCode = "${ExceptionConstant.API_CALL}.tencentcloud",
    code = 500,
    message = "腾讯云服务报错",
    httpStatus = 500,
    api = null,
    serverErrCode = null,
    serverErrMessage = cause?.message ?: "",
    serverErrDescription = null,
    request = null,
    response = null,
    cause = cause
) {
    constructor(e: TencentCloudSDKException) : this(requestId = e.requestId, cause = e) {
        this.cause = e
        this.serverErrCode = e.errorCode
    }
}