package com.rollong.cloud.vendor.tencent.common.rtc

import com.tencentcloudapi.common.Credential
import com.tencentcloudapi.common.profile.ClientProfile
import com.tencentcloudapi.common.profile.HttpProfile
import com.tencentcloudapi.trtc.v20190722.TrtcClient
import com.tencentcloudapi.trtc.v20190722.models.*
import org.slf4j.LoggerFactory

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/08 14:34
 * @project rollong-cloud
 * @filename RtcClient.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.cloud.vendor.tencent.common.rtc
 * @description
 */
class RtcClient(
    private val config: RtcConfig
) {

    companion object {
        private val logger = LoggerFactory.getLogger(RtcClient::class.java)
    }

    private val client: TrtcClient
        get() {
            val credential = Credential(this.config.secretId, this.config.secretKey)
            val httpProfile = HttpProfile()
            httpProfile.reqMethod = "POST"
            httpProfile.connTimeout = 10
            httpProfile.endpoint = this.config.endpoint
            val profile = ClientProfile()
            profile.httpProfile = httpProfile
            profile.signMethod = ClientProfile.SIGN_TC3_256
            return TrtcClient(credential, this.config.region, profile)
        }

    fun removeUser(
        roomId: Long,
        userIds: Set<String>
    ): RemoveUserResponse {
        val request = RemoveUserRequest()
        request.sdkAppId = this.config.app.appId
        request.roomId = roomId
        request.userIds = userIds.toTypedArray()
        return this.client.RemoveUser(request)
    }

    /**
     * @link https://cloud.tencent.com/document/product/647/44270
     * @param streamId 直播流ID，该流ID不能与用户旁路的流ID相同。
     * @param recordId 自定义录制文件名
     * @param audioSampleRate 混流-输出流音频采样率。取值为[96000, 88200, 64000, 48000, 44100, 32000,24000, 22050, 16000, 12000, 11025, 8000]。
     * @param audioChannels 混流-输出流音频声道数，取值范围[1,2]。
     * @param audioChannels    Integer	是	混流-输出流音频声道数，取值范围[1,2]。
     * @param videoWidth    Integer	否	混流-输出流宽，音视频输出时必填。取值范围[0,1920]，单位为像素值。
     * @param videoHeight    Integer	否	混流-输出流高，音视频输出时必填。取值范围[0,1080]，单位为像素值。
     * @param videoBitrate    Integer	否	混流-输出流码率，音视频输出时必填。取值范围[1,10000]，单位为Kbps。
     * @param videoFramerate    Integer	否	混流-输出流帧率，音视频输出时必填。取值为[6,12,15,24,30,48,60]，不在上述帧率值内系统会自动调整。
     * @param videoGop    Integer	否	混流-输出流gop，音视频输出时必填。取值范围[1,5]，单位为秒。
     * @param template 否	混流布局模板ID，0为悬浮模板(默认);1为九宫格模板;2为屏幕分享模板
     * @param mainVideoUserId    String	否	屏幕分享模板中有效，代表左侧大画面对应的用户ID
     * @param mainVideoStreamType    Integer	否	屏幕分享模板中有效，代表左侧大画面对应的流类型，0为摄像头，1为屏幕分享。左侧大画面为web用户时此值填0
     */
    fun startRecord(
        roomId: Long, streamId: String, recordId: String? = null,
        audioSampleRate: Long = 48000, audioChannels: Long = 2,
        videoWidth: Long = 1280, videoHeight: Long = 720, videoBitrate: Long = 1560, videoFramerate: Long = 15, videoGop: Long = 2,
        template: Long = 0, mainVideoUserId: String? = null, mainVideoStreamType: Long = 0
    ): StartMCUMixTranscodeResponse {
        val request = StartMCUMixTranscodeRequest()
        request.roomId = roomId
        request.sdkAppId = this.config.app.appId
        request.outputParams = OutputParams().also {
            it.streamId = streamId
            it.recordId = recordId
        }
        request.encodeParams = EncodeParams().also {
            it.audioSampleRate = audioSampleRate
            it.audioBitrate = audioSampleRate
            it.audioChannels = audioChannels
            it.videoWidth = videoWidth
            it.videoHeight = videoHeight
            it.videoBitrate = videoBitrate
            it.videoFramerate = videoFramerate
            it.videoGop = videoGop
        }
        request.layoutParams = LayoutParams().also {
            it.template = template
            it.mainVideoUserId = mainVideoUserId
            it.mainVideoStreamType = mainVideoStreamType
        }
        return this.client.StartMCUMixTranscode(request)
    }

    /**
     * @link https://cloud.tencent.com/document/product/647/44269
     */
    fun stopRecord(
        roomId: Long
    ): StopMCUMixTranscodeResponse {
        val request = StopMCUMixTranscodeRequest()
        request.roomId = roomId
        request.sdkAppId = this.config.app.appId
        return this.client.StopMCUMixTranscode(request)
    }
}