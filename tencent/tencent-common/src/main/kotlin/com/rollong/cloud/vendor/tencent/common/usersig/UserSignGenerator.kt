package com.rollong.cloud.vendor.tencent.common.usersig

import com.tencentyun.TLSSigAPIv2

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/08 14:49
 * @project rollong-cloud
 * @filename UserSignGenerator.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.cloud.vendor.tencent.common.usersig
 * @description
 */
object UserSignGenerator {

    fun generate(userId: String, appId: Long, appKey: String, expireInSeconds: Long): UserSig {
        val v2 = TLSSigAPIv2(appId, appKey)
        return UserSig(
            appId = appId,
            userId = userId,
            userSig = v2.genUserSig(userId, expireInSeconds),
            expiresAt = expireInSeconds * 1000L + System.currentTimeMillis(),
        )
    }
}