package com.rollong.cloud.vendor.tencent.common.rtc

import com.rollong.cloud.vendor.tencent.common.config.AppConfig

/**
 * Copyright 成都诺朗科技有限公司
 * @author kwang
 * @createdAt 2021/01/08 14:53
 * @project rollong-cloud
 * @filename RtcConfig.kt
 * @ide IntelliJ IDEA
 * @package com.rollong.cloud.vendor.tencent.common.rtc
 * @description
 */
data class RtcConfig(
    var app: AppConfig = AppConfig(),
    var secretId: String = "",
    var secretKey: String = "",
    var region: String = "ap-guangzhou",
    var endpoint: String = "trtc.tencentcloudapi.com"
)